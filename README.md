# Biosup
> This program is designed to download all BIOS/UEFI from either ASUS, ASROCK, GIGABYTE or MSI. 


Biosup is a program designed to automate the sourcing and downloading of BIOS/UEFI from Various vendor websites. Using the config file, a user can manually set what chipset's and vendor's (between ASUS, ASROCK, GIGABYTE and MSI) they wish to download.

Furthermore, Biosup features progress saving and full automation, set and forget!

![](header.PNG)

## Installation
No installation needed :D

to run Biosup on:
Windows:

1. Download the latest version from [here](https://bitbucket.org/Rexzarrax/biosup/downloads/).
2. Unzip the downloaded file.
3. Run BIOSUP.exe.

Linux:

    WINE: detailed steps coming

    Git: detailed steps coming


## Requirements 
Can be found in requirement.txt in main repo [here](https://bitbucket.org/Rexzarrax/biosup/src/master/requirements.txt)

## Credits
Icon made by [Smashicons](https://www.flaticon.com/authors/smashicons) from [Flaticon](https://www.flaticon.com)
